module.exports = {
    configureWebpack: {
        devtool: 'source-map'
    },
    transpileDependencies: [
        'vuetify'
    ],
    pluginOptions: {
        electronBuilder: {
            builderOptions: {
                appId: 'com.afrikaklub.app',
                productName: 'Afrikaklub',
                directories: {
                    output: 'build'
                },
                win: {
                    target: 'NSIS',
                    icon: 'build/icon2.ico'
                }
            }
        }
    }
}
