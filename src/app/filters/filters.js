import Vue from 'vue'
import { DateTime } from 'luxon'

// format nullable content
Vue.filter('nullable', function (value) {
    if (value === undefined || value === null) {
        return 'n/a'
    }
    return value
})

// format raw html content
Vue.filter('sanitize', function (value) {
    return value.toString().replace(/[&<>"']/g, function onReplace (match) {
        return '&#' + match.charCodeAt(0) + ';'
    })
})

// parse datetime from carbon
Vue.filter('datetime', function (date, format = 'ff') {
    if (date !== null) {
        return DateTime.fromISO(date).toFormat(format)
    }
    return 'n/a'
})

// format octet/bytes and convert to B
Vue.filter('bytes', function (bytes) {
    if (bytes !== null) {
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB']
        if (bytes === 0) return '0 Byte'
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)))
        return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i]
    }
    return 'n/a'
})

// truncate raw text
Vue.filter('truncate', function (value, startCharCount = value.length, endCharCount = 0, dot = 3) {
    if (value.length < 20) {
        return value
    }

    if (value !== null) {
        let truncate = ''
        truncate += value.substring(0, startCharCount)
        truncate += '.'.repeat(dot)
        truncate += value.substring(value.length - endCharCount, value.length)

        return truncate
    }
    return 'n/a'
})
