// imports
import http from '@/app/config/http'
import toasted from '@/app/config/toasted'
import tiptap from '@/app/config/tiptap'

export default {
    http: http,
    toasted: toasted,
    tiptap: tiptap
}
