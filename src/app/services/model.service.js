// service
import http from '@/app/config/http'

class Model {
    // get a listing of the resource
    index () {
        return http.get('/models')
    }

    // get the specified resource
    show (id) {
        return http.get('/models/' + id)
    }

    // store a newly created resource in storage
    store (data) {
        return http.post('/models', data)
    }

    // update the specified resource in storage
    update (data) {
        return http.put('/models/' + data.id, data)
    }

    // delete the specified resource in storage
    destroy (id, params) {
        return http.delete('/models/' + id, params)
    }
}

export default new Model()
