import Vue from 'vue'
import router from '@/router/router'
import store from '@/store/store'

// main component
import App from '@/App.vue'

// vuetify
import vuetify from '@/plugins/vuetify/vuetify'

// axios
import axios from 'axios'

// vuelidate
import Vuelidate from 'vuelidate'

// vue toasted
import Toasted from 'vue-toasted'

// lodash
import VueLodash from 'vue-lodash'
import lodash from 'lodash'

// tip tap
import { TiptapVuetifyPlugin } from 'tiptap-vuetify'
import 'tiptap-vuetify/dist/main.css'
import 'vuetify/dist/vuetify.min.css'

// bulma helpers
import 'bulma-helpers/css/bulma-helpers.min.css'

// ----------------------------------
// Application
// ----------------------------------
import '@/app/filters/filters'

// ----------------------------------
// Configuration
// ----------------------------------
import config from '@/app/config/config'

const isDev = process.env.NODE_ENV === 'development'

Vue.config.devtools = isDev
Vue.config.performance = isDev
Vue.config.productionTip = isDev
Vue.http = Vue.prototype.$http = axios

// packages init
Vue.use(Vuelidate)
Vue.use(Toasted, config.toasted)
Vue.use(VueLodash, { lodash: lodash })
Vue.use(TiptapVuetifyPlugin, config.tiptap)

// global properties
Vue.prototype.$base_uri = Vue.http.baseURL
Vue.prototype.$storage_path = isDev ? 'http://api.afrikaklub.local/storage' : 'https://afrikaklub.valutatrust.com/storage'

new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
}).$mount('#app')
