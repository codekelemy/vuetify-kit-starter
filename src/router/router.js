import Vue from 'vue'
import VueRouter from 'vue-router'

// initilize vuerouter
Vue.use(VueRouter)

const routes = [
    // Index
    {
        path: '/',
        name: 'Index',
        component: () => import('@/views/Index.vue'),
    },

    // fallback not found
    {
        path: '*',
        redirect: '/'
    },
]

const router = new VueRouter({
    mode: process.env.IS_ELECTRON ? 'hash' : 'history',
    base: process.env.BASE_URL,
    routes,
    scrollBehavior () {
        return { x: 0, y: 0 }
    }
})

export default router
